import React, {Component} from "react"
import "./List.css"

class Lists extends Component{

  constructor(props){
    super(props)
    this.state = {
     dataHargaBuah : [
      {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500}
    ],
     input:{
       nama: "",
       harga: "",
       berat: ""
     },
     /// array tidak punya index -1
     indexOfForm: -1    
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleChange(event){
    let input = {...this.state.input}
    input[event.targe.name] = event.target.value
    this.setState ({
      input
    });
  }
  handleDelete(event){
    let index = event.target.value
    let newDaftarBuah = this.state.dataHargaBuah
    let editedDaftarBuah = newDaftarBuah[this.state.indexOfForm]
    newDaftarBuah.splice(index, 1)

    if (editedDaftarBuah !== undefined){
      // array findIndex baru ada di ES6
      var newIndex = newDaftarBuah.findIndex((el) => el === editedDaftarBuah)
      this.setState({dataHargaBuah: newDaftarBuah, indexOfForm: newIndex})
      
    }else{
      this.setState({dataHargaBuah: newDaftarBuah})
    }
    
  }
  
  handleEdit(event){
    let index = event.target.value
    let buah = this.state.dataHargaBuah[index]
    this.setState({
      input :{
        nama: buah.nama,
        harga: buah.harga,
        berat: buah.berat
      },
      indexOfForm: index
    })
  }

  

  handleSubmit(event){
    // menahan submit
    event.preventDefault()

    let input = this.state.input

    if (input['nama'].replace(/\s/g,'') !== "" && input['harga'].replace(/\s/g,'') !== "" && input['berat'].replace(/\s/g,'') !== ""){      
      let newDaftarBuah = this.state.dataHargaBuah
      let index = this.state.indexOfForm
      
      if (index === -1){
        newDaftarBuah = [...newDaftarBuah, input]
      }else{
        newDaftarBuah[index] = input
      }
  
      this.setState({
        dataHargaBuah: newDaftarBuah,
        inputName: ""
      })
    }

  }

  render(){
    return(
      <>
        <h1>Daftar Peserta Lomba</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
              {
                this.state.dataHargaBuah.map((el, index)=>{
                  return(                    
                    <tr key={index}>
                      <td>{index+1}</td>
                      <td>{el.nama}</td>.
                      <td>{el.harga}</td>
                      <td>{el.berat}</td>
                      <td>
                        <button onClick={this.handleEdit} value={index}>Edit</button>
                        &nbsp;
                        <button onClick={this.handleDelete} value={index}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
        {/* Form */}
        <h1>Form Buah</h1>
        <form onSubmit={this.handleSubmit}>
          <label>Nama Buah</label> 
          <input type="text" value={this.state.input.nama} onChange={this.handleChange}/><br></br>
          <label>Harga</label> 
          <input type="text" value={this.state.input.harga} onChange={this.handleChange}/><br></br>
          <label>Berat</label> 
          <input type="text" value={this.state.input.berat} onChange={this.handleChange}/><br></br>
          <button>submit</button>
        </form>
      </>
    )
  }
}

export default Lists
